# instaGraminoid_executables
A low-cost, unbiased, high-throughput and extensible colorimetric protocol for quantifying herbicide resistance of individual plants

This repository holds Linux, Mac OS, and Windows standalone executable versions of the core openCV-based image analysis python script. The full instaGraminoid repository can be found [here.](https://gitlab.com/jeffersonfparil/instaGraminoid.git)

## Workflow

<img src="/res/instaGraminoid_method.jpg" width="100%">

## Execute via:

### Windows (64-bit)
Run the executable file, `openCV_plant_colorimetric_windows1064.exe`. Select the folder containing the images (e.g. `instaGraminoid_executables/test`), then enter the case-sensitive image extension name (e.g. jpg), enter the height cropping range separated by a comma (e.g. 0,2700), enter the width cropping range as well (e.g. 0,4928), enter the white balance value (i.e. the expected value of the white background e.g. 200), and finally the plant identification threshold value (e.g. 150 or 75% of the white balance value). The output folder will be generated inside the input folder.

### MacOS X (64-bit)
Allow execution of the executable file, i.e. `chmod +x openCV_plant_colorimetric_mac64bitHS`. Run the executable file, e.g. `./openCV_plant_colorimetric_mac64bitHSt` or double click the file. Select the folder containing the images (e.g. `instaGraminoid_executables/test`), then enter the case-sensitive image extension name (e.g. jpg), enter the height cropping range separated by a comma (e.g. 0,2700), enter the width cropping range as well (e.g. 0,4928), enter the white balance value (i.e. the expected value of the white background e.g. 200), and finally the plant identification threshold value (e.g. 150 or 75% of the white balance value). The output folder will be generated inside the input folder.

### Linux (64-bit)
Run the executable file, e.g. `./openCV_plant_colorimetric_linux64bit`. Select the folder containing the images (e.g. `instaGraminoid_executables/test`), then enter the case-sensitive image extension name (e.g. jpg), enter the height cropping range separated by a comma (e.g. 0,2700), enter the width cropping range as well (e.g. 0,4928), enter the white balance value (i.e. the expected value of the white background e.g. 200), and finally the plant identification threshold value (e.g. 150 or 75% of the white balance value). The output folder will be generated inside the input folder.

**Note**: *These executables will process the images in parallel except for the MacOSX executable. It will iterate across the photographs instead of implementing parallel execution due to Mac-specific bug/s in the multiprocessing and multiprocess modules.*
