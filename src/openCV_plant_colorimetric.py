#!/usr/bin/env python

#######################################
###									###
###  PHENOTYPING USING PHOTOGRAPHS	###
###					OF				###
### 		INDIVIDUAL PLANTS 		###
###		(1 plant per photograph)	###
###									###
####################################### 20180215

########################
###					 ###
### IMPORT LIBRARIES ###
###					 ###
########################
import os, sys, time
import cv2
import numpy as np
import matplotlib
matplotlib.use('Agg') #to allow pyplot savefig to run offline or without X11 and instead use the Agg backend or with nohup [NOTE: must be called before pyplot and pylab] --> comment me out when debugging
from matplotlib import pyplot as plt
import matplotlib.gridspec as gridspec
from scipy.stats import kde
from scipy.stats import norm
from scipy.optimize import minimize
from scipy.integrate import simps
# import seaborn
import Tkinter, tkFileDialog, tkSimpleDialog
from multiprocessing import Pool, freeze_support
from functools import partial

########################
###					 ###
### DEFINE FUNCTIONS ###
###					 ###
########################
##########################################################################################################
#white balance (and color correction)
def __WHITE_BALANCE__(IMAGE, SET_WHITE=255, PLOT=False):
	mean_column = np.mean(IMAGE[0:200,0:200], axis=0) #140:300, 2:220
	mean_row = np.mean(mean_column, axis=0)
	CORRECTION_FACTOR = SET_WHITE/mean_row #setting the white styrofoam box color to 140-140-140
	IMAGE = np.uint8(CORRECTION_FACTOR*IMAGE)
	if PLOT==True:
		plt.imshow(cv2.cvtColor(IMAGE, cv2.COLOR_BGR2RGB)); plt.show()
	return(IMAGE)

#detect plants
def __DETECT_PLANTS__(IMAGE, THRESHOLD, COLOR_COLLAPSE="gray", PLOT=False):
	if COLOR_COLLAPSE=="gray":
		COLLAPSED_IMAGE = cv2.cvtColor(IMAGE, cv2.COLOR_BGR2GRAY)
	elif COLOR_COLLAPSE=="blue":
		COLLAPSED_IMAGE = IMAGE[:,:,0]
	elif COLOR_COLLAPSE=="green":
		COLLAPSED_IMAGE = IMAGE[:,:,1]
	elif COLOR_COLLAPSE=="red":
		COLLAPSED_IMAGE = IMAGE[:,:,2]
	else:
		print("That is not a valid COLOR_COLLAPSE input. Choose from 'gray', 'blue', 'green' and 'red'.")
	thresh, mask1 = cv2.threshold(COLLAPSED_IMAGE, THRESHOLD, 255, cv2.THRESH_BINARY_INV)
	kernel = np.ones((5,5),np.uint8)
	mask2 = cv2.morphologyEx(mask1, cv2.MORPH_OPEN, kernel)
	mask3 = cv2.morphologyEx(mask2, cv2.MORPH_CLOSE, kernel)
	mask4, CONTOURS, hierarchy = cv2.findContours(mask3, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
	IMAGE_EXTRACT = cv2.bitwise_and(IMAGE, IMAGE, mask=mask4)
	if PLOT==True:
			plt.imshow(COLLAPSED_IMAGE, cmap='gray'); plt.show()
			plt.imshow(mask1, cmap='gray'); plt.show()
			plt.imshow(mask2, cmap='gray'); plt.show()
			plt.imshow(mask3, cmap='gray'); plt.show()
			plt.imshow(mask4, cmap='gray'); plt.show()
			plt.imshow(cv2.cvtColor(IMAGE_EXTRACT, cv2.COLOR_BGR2RGB)); plt.show()
	return(IMAGE_EXTRACT, CONTOURS)

#remove pot and shadows, specifically ~gray pixels (be sure to press the soil media so that it won't be visible during phenotyping)
def __REMOVE_GRAYS__(IMAGE, EPSILON=0.10):
	np.seterr(divide='ignore', invalid='ignore')
	TEST = ((1-(IMAGE[:,:, 0].astype('float')/IMAGE[:,:, 1]))<EPSILON) * ((1-(IMAGE[:,:, 0].astype('float')/IMAGE[:,:, 2]))<EPSILON) * ((1-(IMAGE[:,:, 1].astype('float')/IMAGE[:,:, 2]))<EPSILON)
	IMAGE[TEST,:] = [0,0,0]
	return(IMAGE)

def __BUILD_DENSITY__(FLATTENED, PLOT=False):
	DENSITY = kde.gaussian_kde(FLATTENED, bw_method=0.3) #trading bandwidth complexity for speed (bw_method from 0.1 more complex to 0.3 less complex but faster)
	DOMAIN = np.linspace(start=0, stop=255, num=256)
	DENSITY = DENSITY(DOMAIN)
	if PLOT==True:
		plt.plot(DOMAIN, DENSITY); plt.show()
	return DOMAIN, DENSITY

def __AUC__(DOMAIN, DENSITY, START, STOP):
	COMBINED = np.array([DOMAIN, DENSITY])
	x1 = COMBINED[0, DOMAIN>=START]; x1 = x1[x1<(STOP-1)]
	x2 = COMBINED[0, DOMAIN>=(START+1)]; x2 = x2[x2<STOP]
	X = x2-x1
	Y = COMBINED[1, DOMAIN>=START][0:len(X)]
	AUC = sum(X*Y)
	return(AUC)

# def __DENSITYPLOTS_TILE__(IMAGE, BLUE, GREEN, RED, IMAGE_NAME):
# 	plt.figure(1) #figsize not changing the size of the saved image and more that one argument in savetxt fails
# 	G = gridspec.GridSpec(3,2)
# 	plt.subplot(G[:,0])
# 	plt.title("IMAGE")
# 	plt.imshow(cv2.cvtColor(IMAGE, cv2.COLOR_BGR2RGB))
# 	plt.subplot(G[0,1])
# 	plt.title("BLUE")
# 	seaborn.distplot(BLUE) #we can have plt.yscale('linear') or log, symlog or logit + we can also draw grids as plt.grid(True)
# 	plt.subplot(G[1,1])
# 	plt.title("GREEN")
# 	seaborn.distplot(GREEN)
# 	plt.subplot(G[2,1])
# 	plt.title("RED")
# 	seaborn.distplot(RED)
# 	plt.savefig(IMAGE_NAME + '-HISTOGRAMS.png')
# 	#plt.show()
# 	return(0)

#extract plant metrics per individual plant i.e. leaf/whole plant area, AUC in green channel histogram, average pixel values in BGR channels, ....
def __EXTRACT_METRICS_IND__(IMAGE, CONTOURS, THRESH_AREA, IMAGE_NAME, PLOT_DENSITIES=False):
	#remove black pixels
	TEST = (IMAGE[:,:,0]!=0) * (IMAGE[:,:,1]!=0) * (IMAGE[:,:,2]!=0)
	BLUE = TEST * IMAGE[:,:,0]; BLUE = np.reshape(BLUE, (1,np.prod(BLUE.shape))); BLUE = BLUE[BLUE!=0]
	GREEN = TEST * IMAGE[:,:,1]; GREEN = np.reshape(GREEN, (1,np.prod(GREEN.shape))); GREEN = GREEN[GREEN!=0]
	RED = TEST * IMAGE[:,:,2]; RED = np.reshape(RED, (1,np.prod(RED.shape))); RED = RED[RED!=0]
	if np.sum(TEST)<THRESH_AREA:
		METRICS = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
		return(METRICS)
	else:
		#compute metrics
		AREA = sum([cv2.contourArea(x) for x in CONTOURS])
		MEAN_BLUE = np.mean(BLUE)
		MEAN_GREEN = np.mean(GREEN)
		MEAN_RED = np.mean(RED)
		GREEN_INDEX = ( ((1+((MEAN_GREEN-MEAN_RED)/(MEAN_GREEN+MEAN_RED)))/2) + (1-((MEAN_BLUE+MEAN_GREEN+MEAN_RED)/(3*255))) ) / 2
		GREEN_FRACTION = MEAN_GREEN/(MEAN_BLUE + MEAN_GREEN + MEAN_RED)
		DOMAIN_BLUE, DENSITY_BLUE = __BUILD_DENSITY__(BLUE, PLOT=False)
		DOMAIN_GREEN, DENSITY_GREEN = __BUILD_DENSITY__(GREEN, PLOT=False)
		DOMAIN_RED, DENSITY_RED = __BUILD_DENSITY__(RED, PLOT=False)
		# if PLOT_DENSITIES==True:
		# 	__DENSITYPLOTS_TILE__(IMAGE, BLUE, GREEN, RED, IMAGE_NAME)
		AUC_BLUE = __AUC__(DOMAIN_BLUE, DENSITY_BLUE, START=0, STOP=25) #intervals based on eye-ball estimates of histograms across many different plants of varying health
		AUC_GREEN = __AUC__(DOMAIN_GREEN, DENSITY_GREEN, START=100, STOP=125)
		AUC_RED = __AUC__(DOMAIN_RED, DENSITY_RED, START=60, STOP=80)
		#additional metrics of uknown significance
		THRESH_GREEN1 = sum(GREEN>100)/float(len(GREEN))
		THRESH_GREEN2 = sum(GREEN>100)/float(sum(GREEN<100))
		THRESH_RED1 = sum(RED<150)/float(len(RED))
		THRESH_RED2 = sum(RED<150)/float(sum(RED>150))
		MEDIAN_BLUE = np.median(BLUE)
		MEDIAN_GREEN = np.median(GREEN)
		MEDIAN_RED = np.median(RED)
		#output
		METRICS = [AREA, MEAN_BLUE, MEAN_GREEN, MEAN_RED, GREEN_INDEX, GREEN_FRACTION, AUC_BLUE, AUC_GREEN, AUC_RED, THRESH_GREEN1, THRESH_GREEN2, THRESH_RED1, THRESH_RED2, MEDIAN_BLUE, MEDIAN_GREEN, MEDIAN_RED]
		return(METRICS)
##########################################################################################################

########################
###					 ###
###		EXECUTE		 ###
###					 ###
########################

def EXECUTE(height_crop, width_crop, threshold, setWhite, outdir, filename):
	#import the image
	#filename="SAMPLES/DAY14-TRAY03-A1-REP1.jpg"
	#filename="SAMPLES/DAY14-TRAY15-C2-REP1.jpg"
	#filename="SAMPLES/DAY14-TRAY20-B2-REP1.jpg"
	# filename = sys.argv[1]
	# setWhite = int(sys.argv[2]) #check 0:200, 0:200 pixel values in the photographs; may range from 150 to 255
	# threshold = int(sys.argv[3]) #threshold falttened (grayscale) pixel value detected; usually 75% of the setWhite
	img = cv2.imread(filename)
	nrows, ncols, nchannels = img.shape
	# filename = filename.split("/")
	# filename = filename[len(filename)-1]
	filename = os.path.basename(filename)
	# plt.imshow(cv2.cvtColor(img, cv2.COLOR_BGR2RGB)); plt.show()
	#crop
	height_crop = [int(x) for x in height_crop.split(",")]
	width_crop = [int(x) for x in width_crop.split(",")]
	img = img[height_crop[0]:height_crop[1], width_crop[0]:width_crop[1]]
	# img = img[0:2700,:] #align with the lip of pot [2018 04 26 ::: use img = img[0:2400,:] for  SULFOMETURON DAY00 (D063_20180412)]
	# plt.imshow(cv2.cvtColor(img, cv2.COLOR_BGR2RGB)); plt.show()
	#white balance
	# setWhite = 200
	imgWhiteBalanced = __WHITE_BALANCE__(IMAGE=img, SET_WHITE=setWhite, PLOT=False)
	# plt.imshow(cv2.cvtColor(imgWhiteBalanced, cv2.COLOR_BGR2RGB)); plt.show()
	#detect plant
	# threshold = setWhite*0.75 #or setWhite - 10 etc... 
	imgDetected, contours = __DETECT_PLANTS__(IMAGE=imgWhiteBalanced, THRESHOLD=threshold, COLOR_COLLAPSE="gray", PLOT=False)
	# plt.imshow(cv2.cvtColor(imgDetected, cv2.COLOR_BGR2RGB)); plt.show()
	#remove shadows
	imgFiltered = __REMOVE_GRAYS__(IMAGE=imgDetected, EPSILON=0.10)
	# plt.imshow(cv2.cvtColor(imgFiltered, cv2.COLOR_BGR2RGB)); plt.show()
	# imgFiltered[np.where((imgFiltered<[50,50,50]).all(axis=2))]=[255,255,255] # convert background from black to white
	#extract the metrics i.e. area, mean pixel values in blue, green and red channels, and the greeness metric
	metrics = __EXTRACT_METRICS_IND__(IMAGE=imgFiltered, CONTOURS=contours, THRESH_AREA=10000, IMAGE_NAME=filename.split(".",1)[0], PLOT_DENSITIES=False)
	# Yo! Set PLOT_DENSITIES=True initially while we're looking for the mean pixel values for healthy green plants and chlorotic-necrotic sickly plants
	DTYPES=[('name', np.str_, 32), ('area', int), ('blue', float), ('green', float), ('red', float), ('GREEN_INDEX', float), ('GREEN_FRACTION', float), ('auc_blue', float), ('auc_green', float), ('auc_red', float), ('thresh_green1', float), ('thresh_green2', float), ('thresh_red1', float), ('thresh_red2', float), ('media_blue', float), ('media_green', float), ('media_red', float)]
	OUT = []
	OUT.append((filename.split(".",1)[0], metrics[0], metrics[1], metrics[2], metrics[3], metrics[4], metrics[5], metrics[6], metrics[7], metrics[8], metrics[9], metrics[10], metrics[11], metrics[12], metrics[13], metrics[14], metrics[15]))
	OUT = np.array(OUT, dtype=DTYPES)
	#write out
	# np.savetxt(fname=outdir + '/' + filename.split(".",1)[0] + '-out.csv', X=OUT, delimiter=',', fmt=['%s', '%i', '%.4f', '%.4f', '%.4f', '%.4f', '%.4f', '%.4f', '%.4f', '%.4f', '%.4f', '%.4f', '%.4f', '%.4f', '%.4f', '%.4f', '%.4f'], header='ID,AREA,BLUE,GREEN,RED,GREEN_INDEX,GREEN_FRACTION,AUC_BLUE,AUC_GREEN,AUC_RED,THRESH_GREEN1,THRESH_GREEN2,THRESH_RED1,THRESH_RED2,MEDIAN_BLUE,MEDIAN_GREEN,MEDIAN_RED', comments='')
	# cv2.imwrite(outdir + '/' + filename.split(".",1)[0] + '-out.jpg', imgFiltered)
	np.savetxt(fname=os.path.join(outdir, filename.split(".",1)[0]) + '-out.csv', X=OUT, delimiter=',', fmt=['%s', '%i', '%.4f', '%.4f', '%.4f', '%.4f', '%.4f', '%.4f', '%.4f', '%.4f', '%.4f', '%.4f', '%.4f', '%.4f', '%.4f', '%.4f', '%.4f'], header='ID,AREA,BLUE,GREEN,RED,GREEN_INDEX,GREEN_FRACTION,AUC_BLUE,AUC_GREEN,AUC_RED,THRESH_GREEN1,THRESH_GREEN2,THRESH_RED1,THRESH_RED2,MEDIAN_BLUE,MEDIAN_GREEN,MEDIAN_RED', comments='')
	cv2.imwrite(os.path.join(outdir, filename.split(".",1)[0]) + '-out.jpg', imgFiltered)

###########
###		###
### GUI ###
###		###
###########
if __name__ == "__main__":

    ##Add support for when a program which uses multiprocessing has been frozen to produce a Windows executable. (Has been tested with py2exe, PyInstaller and cx_Freeze.)
    #One needs to call this function straight after the '__main__' line of the main module.

	freeze_support() #windows-specific tish!
	
	root = Tkinter.Tk()
	DIRECTORY = tkFileDialog.askdirectory()
	EXTENSION = tkSimpleDialog.askstring("File extension name of the input images", "Enter image extension name:\n  e.g. jpg or png")
	HEIGHT_CROP = tkSimpleDialog.askstring("Crop image by height", "Enter height limits separated by comma (e.g. 800,2400 & 0,2700):")
	WIDTH_CROP = tkSimpleDialog.askstring("Crop image by width", "Enter width limits separated by comma (e.g. 1000,3600 & 0,4928):")
	WHITEBALANCE = tkSimpleDialog.askstring("White balance value", "Enter value (0-255):")
	THRESHOLD = tkSimpleDialog.askstring("Plant detection threshold", "Enter value (0-255):\n usually 75% of the white balance value")
	PLOT = False
	root.withdraw()

	# filename_list = [DIRECTORY + "/" + f for f in os.listdir(DIRECTORY) if f.endswith('.' + EXTENSION)]
	filename_list = [os.path.join(DIRECTORY, f) for f in os.listdir(DIRECTORY) if f.endswith('.' + EXTENSION)]
	# outdir = DIRECTORY + "/OUTPUT" + time.strftime("%Y-%m-%d-%H-%M")
	outdir = os.path.join(DIRECTORY, "OUTPUT") + time.strftime("%Y-%m-%d-%H-%M")
	os.mkdir(outdir)

	print("######################################################################")
	print("Parallel processing...")
	print("######################################################################")

	### parallel processing
	PARALLEL = Pool()
	PARALLEL_OUT = PARALLEL.map(partial(partial(partial(partial(partial(EXECUTE, HEIGHT_CROP), WIDTH_CROP), int(THRESHOLD)), int(WHITEBALANCE)), outdir), filename_list)

	### DONE!
	print("######################################################################")
	print("Execution of the instaGraminoid_executable is successful!")
	print("Results are found in:")
	print(outdir)
	print("######################################################################")
