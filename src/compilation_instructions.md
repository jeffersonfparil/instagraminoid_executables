# Compilation instructions

## Ubuntu 18.04 (64-bit)
    sudo apt install python python-pip
    pip install numpy scipy matplotlib opencv-python multiprocess pyinstaller --user
    pyinstaller --onefile openCV_plant_colorimetric.py

## Mac OS X High Sierra (64-bit)
    sudo easy_install pip
    pip install numpy scipy matplotlib opencv-python multiprocess altgraph==0.16.1 pyinstaller --user
    pyinstaller --onefile openCV_plant_colorimetric.py

## Windows 10 (64-bit)
Download python 2.7 from this [link](https://www.python.org/ftp/python/2.7.15/python-2.7.15.msi) and install it.
Download and install [MS Visual C++](https://www.microsoft.com/en-au/download/details.aspx?id=44266) for the multiprocessing module.
Download pip-Win from this [link.](https://bitbucket.org/pcarbonn/pipwin/downloads/pip-Win_1.9.exe)
Open the pip-Win executable file and enter the following into the "Command:" field:

    venv -c -i pyi-env-name

A command prompt will appear and run:

    pip install numpy scipy matplotlib opencv-python==3.3.0.9 multiprocess altgraph==0.16.1 pyinstaller==3.3.1 --user
    pyinstaller --onefile openCV_plant_colorimetric_no_parallel_for_mac.py
